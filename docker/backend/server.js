const express=require("express")
const utils=require("./utils")
const db=require("./db")

const app=express();

app.use(express.json())


//-------------------------------------------------------------------------------------------------------
                                        //get all movies
//------  -----------------------------------------------------------------------------------------------
app.get("/getAllMovies",(request,response)=>{
    const statement=`SELECT * FROM movie`
    const connection=db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        response.send(utils.showResult(error,data))
    })
})



//-------------------------------------------------------------------------------------------------------
                                        //get movie by name
//------  -----------------------------------------------------------------------------------------------
app.get("/getMovie/:movie",(request,response)=>{
    const {movie}=request.params
    const statement=`SELECT * FROM movie where movie_title='${movie}'`
    const connection=db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        response.send(utils.showResult(error,data))
    })
})


//-------------------------------------------------------------------------------------------------------
                                        //add movie
//------  -----------------------------------------------------------------------------------------------

app.post("/addMovie",(request,response)=>{
    const {movie_title, movie_release_date,movie_time,director_name}=request.body
    const statement=`INSERT INTO movie 
    (movie_title, movie_release_date,movie_time,director_name)
    VALUES
    ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')
    `
    const connection=db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        response.send(utils.showResult(error,data))
    })
})


//-------------------------------------------------------------------------------------------------------
                                        //update movie
//------  -----------------------------------------------------------------------------------------------

app.put("/updateMovie/:movie_id",(request,response)=>{
    const {movie_id}=request.params
    const {movie_release_date,movie_time}=request.body
    const statement=`
    UPDATE movie SET movie_release_date='${movie_release_date}', movie_time='${movie_time}'
    WHERE movie_id=${movie_id}
    `
    const connection=db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        response.send(utils.showResult(error,data))
    })
})


//-------------------------------------------------------------------------------------------------------
                                        //delete movie
//------  -----------------------------------------------------------------------------------------------
app.delete("/deleteMovie/:movie_id",(request,response)=>{
    const {movie_id}=request.params
    const statement=`DELETE FROM movie where movie_id=${movie_id}`
    const connection=db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        response.send(utils.showResult(error,data))
    })
})


app.listen(4000,()=>{
    console.log("server started on port 4000")
})